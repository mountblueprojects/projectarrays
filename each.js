function each(elements,cb){

    if(!Array.isArray(elements)|| typeof cb!=='function'){
        console.log('Error');

        return;
    }

    for(let index=0;index<elements.length;index++){
        console.log(cb(elements,index));
    }

}

module.exports=each;