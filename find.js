function find(elements,cb){

    if(!Array.isArray(elements)||typeof cb!=='function'){

        return [];
    }

    for(let index=0;index<elements.length;index++){
        if(cb(elements,index)===true){
            return elements[index];
        }
    }

    return undefined;
}

module.exports=find;