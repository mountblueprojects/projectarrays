const array1 = [1, 2, 3, 4, 5];
const initialValue = 0;
const reduce = require("../reduce");

const result = reduce(
  array1,
  (accu, curr) => {
    return accu + curr * curr * curr;
  },
  100
);

console.log(result);
