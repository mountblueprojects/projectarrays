const numbers = ["1", "2", "3"];
const map = require("../map");
const squares = (element) => element * element;
const result = numbers.map(parseInt);

if (result.length === 0) {
  console.log("Enter a proper array or function");
} else {
  console.log(result);
}
