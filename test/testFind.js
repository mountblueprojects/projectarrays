const items = [1, 2, 3, 4, 5, 5];

const find=require('../find');

const result=find(items,(array,index)=>{
    
    return array[index]===4;
});

if(typeof result==='undefined'){
    console.log("Element not Found");
} else if(result.length===0){
    console.log("Enter proper array and/or function")
} else{
    console.log(result);
}