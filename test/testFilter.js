const words = [
  "spray",
  "limit",
  "elite",
  "exuberant",
  "destruction",
  "present",
];

const filter = require("../filter");

const result = filter(words, (word) => {
  return word.length > 6;
});

if (result === 0) {
  console.log("Enter proper array and/or function");
} else if (result.length === 0) {
  console.log("No element satisfies the condition");
} else {
  console.log(result);
}
