function filter(elements, cb) {
  if (Array.isArray(elements) && typeof cb === "function") {
    let result = [];

    for (let index = 0; index < elements.length; index++) {
      if (cb(elements[index], index, elements) === true) {
        result.push(elements[index]);
      }
    }

    return result;
  } else {
    return 0;
  }
}

module.exports = filter;
