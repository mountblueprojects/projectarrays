function reduce(elements, cb, initialValue) {
  if (!Array.isArray(elements) || typeof cb !== "function") {
    return [];
  }

  let index;

  if (initialValue === undefined) {
    initialValue = elements[0];
    index = 1;
  } else {
    index = 0;
  }

  for (index; index < elements.length; index++) {
    initialValue = cb(initialValue, elements[index], index, elements);
  }

  return initialValue;
}

module.exports = reduce;
