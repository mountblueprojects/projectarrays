function map(elements, cb) {
  if (Array.isArray(elements) && typeof cb === "function") {
    let result = [];

    for (let index = 0; index < elements.length; index++) {
      result.push(cb(elements[index], index, elements));
    }

    return result;
  } else {
    return [];
  }
}

module.exports = map;
